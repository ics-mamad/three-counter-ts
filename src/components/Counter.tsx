import React, { useState } from "react";

interface TestCounter{
    count: number,
    message?: string
  }

const Counter: React.FC<TestCounter> = ({count,message}) => {
    const [Counter1Val, setCounter1Val] = useState<number>(0);
    const [Counter1Interval, setCounter1Interval] = useState<number | undefined>();
    const [Counter2Val, setCounter2Val] = useState<number>(0);
    const [Counter2Interval, setCounter2Interval] = useState<number | undefined>();
    const [Counter3Val, setCounter3Val] = useState<number>(0);
    const [Counter3Interval, setCounter3Interval] = useState<number | undefined>();

    

    // Counter-1
    const increment1 = (): void => {
        setCounter1Val((Counter1Val) => Counter1Val + 1);
    };
    const counter1 = (): void => {
        setCounter1Interval(setInterval(increment1, 400));
    };
    const stop1 = ():void => {
        if (Counter1Val !== 0) {
            clearInterval(Counter1Interval);
        } else {
            alert("Start counter before Stop");
        }
    };
    const resume1 = ():void => {
        if (Counter1Val !== 0) {
            setCounter1Interval(setInterval(increment1, 400));
        } else {
            alert("Can't Resume Before Start");
        }
    };
    const reset1 = ():void => {
        if (Counter1Val === 0) {
            alert("Counter Is Already At 0");
        } else {
            clearInterval(Counter1Interval);
            setCounter1Val(0);
        }
    };

    // Counter-2
    const increment2 = ():void => {
        setCounter2Val((Counter2Val) => Counter2Val + 1);
    };
    const counter2 = ():void => {
        setCounter2Interval(setInterval(increment2, 400));
    };
    const stop2 = ():void => {
        if (Counter2Val !== 0) {
            clearInterval(Counter2Interval);
        } else {
            alert("Start counter before Stop");
        }
    };
    const resume2 = ():void => {
        if (Counter2Val !== 0) {
            setCounter2Interval(setInterval(increment2, 400));
        } else {
            alert("Can't Resume Before Start");
        }
    };
    const reset2 = ():void => {
        if (Counter2Val === 0) {
            alert("Counter Is Already At 0");
        } else {
            clearInterval(Counter2Interval);
            setCounter2Val(0);
        }
    };

    // Counter-3
    const increment3 = ():void => {
        setCounter3Val((Counter3Val) => Counter3Val + 1);
    };
    const counter3 = ():void => {
        setCounter3Interval(setInterval(increment3, 400));
    };
    const stop3 = ():void => {
        if (Counter3Val !== 0) {
            clearInterval(Counter3Interval);
        } else {
            alert("Start counter before Stop");
        }
    };
    const resume3 = ():void => {
        if (Counter3Val !== 0) {
            setCounter3Interval(setInterval(increment3, 400));
        } else {
            alert("Can't Resume Before Start");
        }
    };
    const reset3 = ():void => {
        if (Counter3Val === 0) {
            alert("Counter Is Already At 0");
        } else {
            clearInterval(Counter3Interval);
            setCounter3Val(0);
        }
    };

    // General buttons
    const genstart = ():void => {
        counter1();
        counter2();
        counter3();
    };
    const genstop = ():void => {
        stop1();
        stop2();
        stop3();
    };
    const genresume = ():void => {
        resume1();
        resume2();
        resume3();
    };
    const genreset = ():void => {
        reset1();
        reset2();
        reset3();
    };

    return (
        <>
        <h1>This Is Test Counter : {count} with optional :{message}</h1>
            <div>
                {Counter1Val}
                <button onClick={counter1}>Start</button>
                <button onClick={stop1}>Stop</button>
                <button onClick={resume1}>Resume</button>
                <button onClick={reset1}>Reset</button>
            </div>
            <br />
            <div>
                {Counter2Val}
                <button onClick={counter2}>Start</button>
                <button onClick={stop2}>Stop</button>
                <button onClick={resume2}>Resume</button>
                <button onClick={reset2}>Reset</button>
            </div>
            <br />
            <div>
                {Counter3Val}
                <button onClick={counter3}>Start</button>
                <button onClick={stop3}>Stop</button>
                <button onClick={resume3}>Resume</button>
                <button onClick={reset3}>Reset</button>
            </div>
            <br />
            <div>
                <button onClick={genstart}>Start</button>
                <button onClick={genstop}>Stop</button>
                <button onClick={genresume}>Resume</button>
                <button onClick={genreset}>Reset</button>
            </div>
        </>
    );
};

export default Counter;
